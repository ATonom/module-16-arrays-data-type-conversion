﻿// Module 16. Arrays. Data type conversion.cpp

#include <iostream>
#include <iomanip>
#include <ctime>

/*
int enterDate()
{
	int currentDate = 0;
	bool isCorrect = false;

	do
	{
		std::cout << std::endl << "Enter the date of the current month: ";
		std::cin >> currentDate;

		if ((currentDate > 1) && (currentDate < 31)) isCorrect = true;

	} while (!isCorrect);

	return currentDate;
}
*/

int main()
{
	const int size = 5;
	int array[size][size]{};

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
			std::cout << std::setw(3) << array[i][j];
		}

		std::cout << std::endl;
	}

	struct tm newtime;
	time_t now = time(0);
	localtime_s(&newtime, &now);

	int day = newtime.tm_mday;
		
	for (int i = 0; i < size; i++)
	{
		if (i == (day % size))
		{
			int sumOfRowElements = 0;

			for (int j = 0; j < size; j++) sumOfRowElements += array[i][j];

			std::cout << "The sum of the line elements is " << sumOfRowElements << std::endl;
		}
	}
}

/*
1.	В главном исполняемом файле (файл, в котором находится функция main) создайте двумерный
массив размерности NxN и заполните его так, чтобы элемент с индексами i и j был равен i + j.
2.	Выведите этот массив в консоль.
3.	Выведите сумму элементов в строке массива, индекс которой равен остатку деления текущего числа
календаря на N (в двумерном массиве a[i][j], i — индекс строки).


*/